#include <stream9/linux/fd.hpp>

#include <ostream>

#include <stream9/cstring_ptr.hpp>
#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/log.hpp>
#include <stream9/source_location.hpp>
#include <stream9/string_view.hpp>
#include <stream9/to_string.hpp>

#include <fcntl.h>
#include <sys/stat.h>

namespace stream9::linux {

static fd
open(cstring_ptr const& pathname, int flags)
{
    auto rc = ::open(pathname, flags, 0666);
    if (rc == -1) {
        throw error {
            "open(2)",
            lx::make_error_code(errno),
        };
    }
    else {
        return fd(rc);
    }
}

static void
close(int fd)
{
    if (fd != -1) {
        if (::close(fd) == -1) {
            throw error {
                "close(2)",
                lx::make_error_code(errno),
                { { "fd", fd } }
            };
        }
    }
}

static void
close_or_log(int fd, source_location where = {} )
{
    try {
        close(fd);
    }
    catch (...) {
        print_error(log::dbg(), std::move(where));
    }
}

static int
do_fcntl(int fd, int cmd, std::string_view cmd_sym)
{
    auto rv = ::fcntl(fd, cmd);
    if (rv == -1) {
        throw error {
            "fcntl(2)",
            lx::make_error_code(errno), {
                { "fildes", fd },
                { "cmd", cmd_sym },
            }
        };
    }
    return rv;
}

static string
readlinkat(fd_ref dirfd, cstring_ptr const& pathname)
{
    try {
        char buf[PATH_MAX];

        auto n = ::readlinkat(dirfd, pathname, buf, sizeof(buf));
        if (n == -1) {
            throw error {
                "readlinkat(2)",
                lx::make_error_code(errno),
            };
        }
        else {
            return string_view(buf, n);
        }
    }
    catch (...) {
        rethrow_error({
            { "dirfd", dirfd },
            { "pathname", pathname },
        });
    }
}

/*
 * class fd
 */
fd::
~fd() noexcept
{
    close_or_log(m_fd);
}

fd& fd::
operator=(fd&& other) noexcept
{
    close_or_log(m_fd);

    m_fd = other.m_fd;
    other.m_fd = -1;

    return *this;
}

int fd::
descriptor_flags() const
{
    try {
        return do_fcntl(m_fd, F_GETFD, "F_GETFD");
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

int fd::
status_flags() const
{
    try {
        return do_fcntl(m_fd, F_GETFL, "F_GETFL");
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd::
close_on_exec() const
{
    try {
        return descriptor_flags() & FD_CLOEXEC;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd::
nonblocking_mode() const
{
    try {
        return status_flags() & O_NONBLOCK;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

int fd::
access_mode() const
{
    try {
        return status_flags() & O_ACCMODE;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

string fd::
path() const
{
    try {
        auto dirfd = open("/proc/self/fd", O_RDONLY);

        auto filename = str::to_string(m_fd);

        return readlinkat(dirfd, filename);
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd::
is_open() const
{
    try {
        return descriptor_flags() != -1;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

void fd::
set_descriptor_flags(int f)
{
    try {
        auto rc = ::fcntl(m_fd, F_SETFD, f);
        if (rc == -1) {
            throw error {
                "fcntl(2)",
                lx::make_error_code(errno), {
                    { "op", "F_SETFD" },
                    { "flags", f }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({{ "fd", m_fd }});
    }
}

void fd::
set_status_flags(int f)
{
    try {
        auto rc = ::fcntl(m_fd, F_SETFL, f);
        if (rc == -1) {
            throw error {
                "fcntl(2)",
                lx::make_error_code(errno), {
                    { "op", "F_SETFL" },
                    { "flags", f }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({{ "fd", m_fd }});
    }
}

void fd::
set_close_on_exec(bool on)
{
    try {
        auto x = descriptor_flags();
        int y = on ? (x | FD_CLOEXEC) : (x & ~FD_CLOEXEC);

        set_descriptor_flags(y);
    }
    catch (...) {
        rethrow_error();
    }
}

void fd::
set_nonblocking_mode(bool on)
{
    try {
        auto x = status_flags();
        int y = on ? (x | O_NONBLOCK) : (x & ~O_NONBLOCK);

        set_status_flags(y);
    }
    catch (...) {
        rethrow_error();
    }
}

void fd::
close()
{
    try {
        linux::close(m_fd);
        m_fd = -1;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

/*
 * class fd_ref
 */
int fd_ref::
descriptor_flags() const
{
    try {
        return do_fcntl(m_fd, F_GETFD, "F_GETFD");
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

int fd_ref::
status_flags() const
{
    try {
        return do_fcntl(m_fd, F_GETFL, "F_GETFL");
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd_ref::
close_on_exec() const
{
    try {
        return descriptor_flags() & FD_CLOEXEC;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd_ref::
nonblocking_mode() const
{
    try {
        return status_flags() & O_NONBLOCK;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

int fd_ref::
access_mode() const
{
    try {
        return status_flags() & O_ACCMODE;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

string fd_ref::
path() const
{
    try {
        auto dirfd = open("/proc/self/fd", O_RDONLY);

        auto filename = str::to_string(m_fd);

        return readlinkat(dirfd, filename);
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

bool fd_ref::
is_open() const
{
    try {
        return descriptor_flags() != -1;
    }
    catch (...) {
        rethrow_error({ { "fd", m_fd } });
    }
}

void fd_ref::
set_descriptor_flags(int f)
{
    try {
        auto rc = ::fcntl(m_fd, F_SETFD, f);
        if (rc == -1) {
            throw error {
                "fcntl(2)",
                lx::make_error_code(errno), {
                    { "op", "F_SETFD" },
                    { "flags", f }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({{ "fd", m_fd }});
    }
}

void fd_ref::
set_status_flags(int f)
{
    try {
        auto rc = ::fcntl(m_fd, F_SETFL, f);
        if (rc == -1) {
            throw error {
                "fcntl(2)",
                lx::make_error_code(errno), {
                    { "op", "F_SETFL" },
                    { "flags", f }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({{ "fd", m_fd }});
    }
}

void fd_ref::
set_close_on_exec(bool on)
{
    try {
        auto x = descriptor_flags();
        int y = on ? (x | FD_CLOEXEC) : (x & ~FD_CLOEXEC);

        set_descriptor_flags(y);
    }
    catch (...) {
        rethrow_error();
    }
}

void fd_ref::
set_nonblocking_mode(bool on)
{
    try {
        auto x = status_flags();
        int y = on ? (x | O_NONBLOCK) : (x & ~O_NONBLOCK);

        set_status_flags(y);
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * auxiliary functions
 */
std::ostream&
operator<<(std::ostream& os, fd const& fd)
{
    return os << static_cast<int>(fd);
}

void
tag_invoke(json::value_from_tag, json::value& v, fd const& fd)
{
    v = static_cast<int>(fd);
}

std::ostream&
operator<<(std::ostream& os, fd_ref const fd)
{
    return os << static_cast<int>(fd);
}

void
tag_invoke(json::value_from_tag, json::value& v, fd_ref const fd)
{
    v = static_cast<int>(fd);
}

} // namespace stream9::linux
