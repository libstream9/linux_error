#include <stream9/linux/error.hpp>

#include <string>

namespace stream9::linux {

namespace {

struct entry
{
    char const* const symbol { nullptr };
    char const* const description { nullptr };
    std::errc condition {};
};

using enum std::errc;

static entry entry_tbl[] {
    /* 0 */ { },
    /* 1 */ { "EPERM", "Operation not permitted", operation_not_permitted },
    /* 2 */ { "ENOENT", "No such file or directory", no_such_file_or_directory },
    /* 3 */ { "ESRCH", "No such process", no_such_process },
    /* 4 */ { "EINTR", "Interrupted system call", interrupted },
    /* 5 */ { "EIO", "I/O error", io_error },
    /* 6 */ { "ENXIO", "No such device or address", no_such_device_or_address },
    /* 7 */ { "E2BIG", "Argument list too long", argument_list_too_long },
    /* 8 */ { "ENOEXEC", "Exec format error", executable_format_error },
    /* 9 */ { "EBADF", "Bad file number", bad_file_descriptor },
    /* 10 */ { "ECHILD", "No child processes", no_child_process },
    /* 11 */ { "EAGAIN (EWOULDBLOCK)", "Try again (Operation would block)", resource_unavailable_try_again },
    /* 12 */ { "ENOMEM", "Out of memory", not_enough_memory },
    /* 13 */ { "EACCES", "Permission denied", permission_denied },
    /* 14 */ { "EFAULT", "Bad address", bad_address },
    /* 15 */ { "ENOTBLK", "Block device required", {} },
    /* 16 */ { "EBUSY", "Device or resource busy", device_or_resource_busy },
    /* 17 */ { "EEXIST", "File exists", file_exists },
    /* 18 */ { "EXDEV", "Cross-device link", cross_device_link },
    /* 19 */ { "ENODEV", "No such device", no_such_device },
    /* 20 */ { "ENOTDIR", "Not a directory", not_a_directory },
    /* 21 */ { "EISDIR", "Is a directory", is_a_directory },
    /* 22 */ { "EINVAL", "Invalid argument", invalid_argument },
    /* 23 */ { "ENFILE", "File table overflow", too_many_files_open_in_system },
    /* 24 */ { "EMFILE", "Too many open files", too_many_files_open },
    /* 25 */ { "ENOTTY", "Not a typewriter", inappropriate_io_control_operation },
    /* 26 */ { "ETXTBSY", "Text file busy", text_file_busy },
    /* 27 */ { "EFBIG", "File too large", file_too_large },
    /* 28 */ { "ENOSPC", "No space left on device", no_space_on_device },
    /* 29 */ { "ESPIPE", "Illegal seek", invalid_seek },
    /* 30 */ { "EROFS", "Read-only file system", read_only_file_system },
    /* 31 */ { "EMLINK", "Too many links", too_many_links },
    /* 32 */ { "EPIPE", "Broken pipe", broken_pipe },
    /* 33 */ { "EDOM", "Math argument out of domain of func", argument_out_of_domain },
    /* 34 */ { "ERANGE", "Math result not representable", result_out_of_range },
    /* 35 */ { "EDEADLK", "Resource deadlock would occur", resource_deadlock_would_occur },
    /* 36 */ { "ENAMETOOLONG", "File name too long", filename_too_long },
    /* 37 */ { "ENOLCK", "No record locks available", no_lock_available },
    /* 38 */ { "ENOSYS", "Invalid system call number", function_not_supported },
    /* 39 */ { "ENOTEMPTY", "Directory not empty", directory_not_empty },
    /* 40 */ { "ELOOP", "Too many symbolic links encountered", too_many_symbolic_link_levels },
    /* 41 */ {},
    /* 42 */ { "ENOMSG", "No message of desired type", no_message },
    /* 43 */ { "EIDRM", "Identifier removed", identifier_removed },
    /* 44 */ { "ECHRNG", "Channel number out of range", {} },
    /* 45 */ { "EL2NSYNC", "Level 2 not synchronized", {} },
    /* 46 */ { "EL3HLT", "Level 3 halted", {} },
    /* 47 */ { "EL3RST", "Level 3 reset", {} },
    /* 48 */ { "ELNRNG", "Link number out of range", {} },
    /* 49 */ { "EUNATCH", "Protocol driver not attached", {} },
    /* 50 */ { "ENOCSI", "No CSI structure available", {} },
    /* 51 */ { "EL2HLT", "Level 2 halted", {} },
    /* 52 */ { "EBADE", "Invalid exchange", {} },
    /* 53 */ { "EBADR", "Invalid request descriptor", {} },
    /* 54 */ { "EXFULL", "Exchange full", {} },
    /* 55 */ { "ENOANO", "No anode", {} },
    /* 56 */ { "EBADRQC", "Invalid request code", {} },
    /* 57 */ { "EBADSLT", "Invalid slot", {} },
    /* 58 */ {},
    /* 59 */ { "EBFONT", "Bad font file format", {} },
    /* 60 */ { "ENOSTR", "Device not a stream", not_a_stream },
    /* 61 */ { "ENODATA", "No data available", no_message_available },
    /* 62 */ { "ETIME", "Timer expired", stream_timeout },
    /* 63 */ { "ENOSR", "Out of streams resources", no_stream_resources },
    /* 64 */ { "ENONET", "Machine is not on the network", {} },
    /* 65 */ { "ENOPKG", "Package not installed", {} },
    /* 66 */ { "EREMOTE", "Object is remote", {} },
    /* 67 */ { "ENOLINK", "Link has been severed", no_link },
    /* 68 */ { "EADV", "Advertise error", {} },
    /* 69 */ { "ESRMNT", "Srmount error", {} },
    /* 70 */ { "ECOMM", "Communication error on send", {} },
    /* 71 */ { "EPROTO", "Protocol error", protocol_error },
    /* 72 */ { "EMULTIHOP", "Multihop attempted", {} },
    /* 73 */ { "EDOTDOT", "RFS specific error", {} },
    /* 74 */ { "EBADMSG", "Not a data message", bad_message },
    /* 75 */ { "EOVERFLOW", "Value too large for defined data type", value_too_large },
    /* 76 */ { "ENOTUNIQ", "Name not unique on network", {} },
    /* 77 */ { "EBADFD", "File descriptor in bad state", {} },
    /* 78 */ { "EREMCHG", "Remote address changed", {} },
    /* 79 */ { "ELIBACC", "Can not access a needed shared library", {} },
    /* 80 */ { "ELIBBAD", "Accessing a corrupted shared library", {} },
    /* 81 */ { "ELIBSCN", ".lib section in a.out corrupted", {} },
    /* 82 */ { "ELIBMAX", "Attempting to link in too many shared libraries", {} },
    /* 83 */ { "ELIBEXEC", "Cannot exec a shared library directly", {} },
    /* 84 */ { "EILSEQ", "Illegal byte sequence", {} },
    /* 85 */ { "ERESTART", "Interrupted system call should be restarted", {} },
    /* 86 */ { "ESTRPIPE", "Streams pipe error", {} },
    /* 87 */ { "EUSERS", "Too many users", {} },
    /* 88 */ { "ENOTSOCK", "Socket operation on non-socket", not_a_socket },
    /* 89 */ { "EDESTADDRREQ", "Destination address required", destination_address_required },
    /* 90 */ { "EMSGSIZE", "Message too long", message_size },
    /* 91 */ { "EPROTOTYPE", "Protocol wrong type for socket", wrong_protocol_type },
    /* 92 */ { "ENOPROTOOPT", "Protocol not available", wrong_protocol_type },
    /* 93 */ { "EPROTONOSUPPORT", "Protocol not supported", protocol_not_supported },
    /* 94 */ { "ESOCKTNOSUPPORT", "Socket type not supported", {} },
    /* 95 */ { "EOPNOTSUPP", "Operation not supported on transport endpoint", operation_not_supported },
    /* 96 */ { "EPFNOSUPPORT", "Protocol family not supported", {} },
    /* 97 */ { "EAFNOSUPPORT", "Address family not supported by protocol", },
    /* 98 */ { "EADDRINUSE", "Address already in use", address_in_use },
    /* 99 */ { "EADDRNOTAVAIL", "Cannot assign requested address", address_not_available },
    /* 100 */ { "ENETDOWN", "Network is down", network_down },
    /* 101 */ { "ENETUNREACH", "Network is unreachable", network_unreachable },
    /* 102 */ { "ENETRESET", "Network dropped connection because of reset", network_reset },
    /* 103 */ { "ECONNABORTED", "Software caused connection abort", connection_aborted },
    /* 104 */ { "ECONNRESET", "Connection reset by peer", connection_reset },
    /* 105 */ { "ENOBUFS", "No buffer space available", no_buffer_space },
    /* 106 */ { "EISCONN", "Transport endpoint is already connected", already_connected },
    /* 107 */ { "ENOTCONN", "Transport endpoint is not connected", not_connected },
    /* 108 */ { "ESHUTDOWN", "Cannot send after transport endpoint shutdown", {} },
    /* 109 */ { "ETOOMANYREFS", "Too many references: cannot splice", {} },
    /* 110 */ { "ETIMEDOUT", "Connection timed out", timed_out },
    /* 111 */ { "ECONNREFUSED", "Connection refused", connection_refused },
    /* 112 */ { "EHOSTDOWN", "Host is down", {} },
    /* 113 */ { "EHOSTUNREACH", "No route to host", host_unreachable },
    /* 114 */ { "EALREADY", "Operation already in progress", connection_already_in_progress },
    /* 115 */ { "EINPROGRESS", "Operation now in progress", operation_in_progress },
    /* 116 */ { "ESTALE", "Stale file handle", {} },
    /* 117 */ { "EUCLEAN", "Structure needs cleaning", {} },
    /* 118 */ { "ENOTNAM", "Not a XENIX named type file", {} },
    /* 119 */ { "ENAVAIL", "No XENIX semaphores available", {} },
    /* 120 */ { "EISNAM", "Is a named type file", {} },
    /* 121 */ { "EREMOTEIO", "Remote I/O error", {} },
    /* 122 */ { "EDQUOT", "Quota exceeded", {} },
    /* 123 */ { "ENOMEDIUM", "No medium found", {} },
    /* 124 */ { "EMEDIUMTYPE", "Wrong medium type", {} },
    /* 125 */ { "ECANCELED", "Operation Canceled", operation_canceled },
    /* 126 */ { "ENOKEY", "Required key not available", {} },
    /* 127 */ { "EKEYEXPIRED", "Key has expired", {} },
    /* 128 */ { "EKEYREVOKED", "Key has been revoked", {} },
    /* 129 */ { "EKEYREJECTED", "Key was rejected by service", {} },
    /* 130 */ { "EOWNERDEAD", "Owner died", owner_dead },
    /* 131 */ { "ENOTRECOVERABLE", "State not recoverable", state_not_recoverable },
    /* 132 */ { "ERFKILL", "Operation not possible due to RF-kill", {} },
    /* 133 */ { "EHWPOISON", "Memory page has hardware error", {} },
};

constexpr auto errno_max = static_cast<int>(sizeof(entry_tbl) / sizeof(entry)) - 1;

static void
unknown_error(std::ostream& os, int const e)
{
    os << "unknown error: " << e;
}

} // anynymous namespace

class error_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "stream9::linux";
    }

    std::string message(int const condition) const override
    {
        std::ostringstream oss;

        if (condition < 0 || condition > errno_max) {
            unknown_error(oss, condition);
        }
        else {
            auto const& entry = entry_tbl[condition];
            if (!entry.symbol) {
                unknown_error(oss, condition);
            }
            else {
                oss << entry.symbol << ": " << entry.description;
            }
        }

        return oss.str();
    }

    bool
    equivalent(int const code,
               std::error_condition const& condition) const noexcept override
    {
        if (condition.category() == *this) {
            return code == condition.value();
        }
        else if (condition.category() == std::generic_category()) {
            if (code == EAGAIN) {
                return condition == std::errc::resource_unavailable_try_again
                    || condition == std::errc::operation_would_block;
            }
            else if (code < 0 || code > errno_max) {
                return false;
            }
            else {
                return condition == entry_tbl[code].condition;
            }
        }
        else {
            return false;
        }
    }
};

std::error_category const&
error_category()
{
    static error_category_impl category;

    return category;
}

} // namespace stream9::linux
