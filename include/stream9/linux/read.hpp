#ifndef STREAM9_LINUX_FILE_READ_HPP
#define STREAM9_LINUX_FILE_READ_HPP

#include "error_code.hpp"
#include "fd.hpp"
#include "namespace.hpp"

#include <stream9/array_view.hpp>
#include <stream9/number.hpp>
#include <stream9/outcome.hpp>

#include <concepts>

#include <unistd.h>

namespace stream9::linux {

outcome<natural<ssize_t>, lx::errc>
read(fd_ref fd, array_view<char> buf) noexcept;

template<typename T>
outcome<natural<ssize_t>, lx::errc>
read(fd_ref fd, T& buf) noexcept
    requires (!std::convertible_to<T, array_view<char>>);

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_READ_HPP

#include "core/read.ipp"
