#ifndef STREAM9_LINUX_IOVEC_HPP
#define STREAM9_LINUX_IOVEC_HPP

#include <stream9/array_view.hpp>
#include <stream9/string_view.hpp>

#include <concepts>

#include <sys/uio.h>

namespace stream9::linux {

struct iovec : ::iovec
{
    iovec(void* base, size_t len) noexcept
        : ::iovec { base, len }
    {}

    iovec(array_view<char> s) noexcept
        : iovec { s.data(), s.size() }
    {}

    template<typename T>
    iovec(T&& s) noexcept
        requires std::convertible_to<T, array_view<char>>
        : iovec { array_view<char>(s) }
    {}

    template<typename T>
    iovec(T&& s) noexcept
        requires (!std::convertible_to<T, array_view<char>>)
              && (!std::is_const_v<T>)
        : iovec { &s, sizeof(s) }
    {}
};

struct const_iovec : ::iovec
{
    const_iovec(void const* base, size_t len) noexcept
        : ::iovec { const_cast<void*>(base), len }
    {}

    const_iovec(array_view<char const> s) noexcept
        : const_iovec { s.data(), s.size() }
    {}

    template<size_t N>
    const_iovec(char const (&s)[N]) noexcept
        : const_iovec(string_view(s))
    {}

    template<typename T>
    const_iovec(T&& s) noexcept
        requires std::convertible_to<T, array_view<char const>>
        : const_iovec { array_view<char const>(s) }
    {}

    template<typename T>
    const_iovec(T&& s)
        requires (!std::convertible_to<T, array_view<char const>>)
        : const_iovec { &s, sizeof(s) }
    {}
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_IOVEC_HPP
