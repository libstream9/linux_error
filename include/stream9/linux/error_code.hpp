#ifndef STREAM9_LINUX_ERROR_ERROR_CODE_HPP
#define STREAM9_LINUX_ERROR_ERROR_CODE_HPP

#include "namespace.hpp"

#include <system_error>

#include <errno.h>

namespace stream9::linux {

enum errc {
    ok = 0,
    // copy from linux 5.17.1
    // /usr/include/asm-generic/error-base.h
    eperm = EPERM,        /* 1: Operation not permitted */
    enoent = ENOENT,      /* 2: No such file or directory */
    esrch = ESRCH,        /* 3: No such process */
    eintr = EINTR,        /* 4: Interrupted system call */
    eio = EIO,            /* 5: I/O error */
    enxio = ENXIO,        /* 6: No such device or address */
    e2big = E2BIG,        /* 7: Argument list too long */
    enoexec = ENOEXEC,    /* 8: Exec format error */
    ebadf = EBADF,        /* 9: Bad file number */
    echild = ECHILD,      /* 10: No child processes */
    eagain = EAGAIN,      /* 11: Try again */
    enomem = ENOMEM,      /* 12: Out of memory */
    eacces = EACCES,      /* 13: Permission denied */
    efault = EFAULT,      /* 14: Bad address */
    enotblk = ENOTBLK,    /* 15: Block device required */
    ebusy = EBUSY,        /* 16: Device or resource busy */
    eexist = EEXIST,      /* 17: File exists */
    exdev = EXDEV,        /* 18: Cross-device link */
    enodev = ENODEV,      /* 19: No such device */
    enotdir = ENOTDIR,    /* 20: Not a directory */
    eisdir = EISDIR,      /* 21: Is a directory */
    einval = EINVAL,      /* 22: Invalid argument */
    enfile = ENFILE,      /* 23: File table overflow */
    emfile = EMFILE,      /* 24: Too many open files */
    enotty = ENOTTY,      /* 25: Not a typewriter */
    etxtbsy = ETXTBSY,    /* 26: Text file busy */
    efbig = EFBIG,        /* 27: File too large */
    enospc = ENOSPC,      /* 28: No space left on device */
    espipe = ESPIPE,      /* 29: Illegal seek */
    erofs = EROFS,        /* 30: Read-only file system */
    emlink = EMLINK,      /* 31: Too many links */
    epipe = EPIPE,        /* 32: Broken pipe */
    edom = EDOM,          /* 33: Math argument out of domain of func */
    erange = ERANGE,      /* 34: Math result not representable */

    // /usr/include/asm-generic/errno.h
    edeadlk = EDEADLK, /* 35: Resource deadlock would occur */
    enametoolong = ENAMETOOLONG, /* 36: File name too long */
    enolck = ENOLCK, /* 37: No record locks available */
    enosys = ENOSYS, /* 38: Invalid system call number */
    enotempty = ENOTEMPTY, /* 39: Directory not empty */
    eloop = ELOOP, /* 40: Too many symbolic links encountered */
    ewouldblock = EWOULDBLOCK, /* EAGAIN: Operation would block */
    enomsg = ENOMSG, /* 42: No message of desired type */
    eidrm = EIDRM, /* 43: Identifier removed */
    echrng = ECHRNG, /* 44: Channel number out of range */
    el2nsync = EL2NSYNC, /* 45: Level 2 not synchronized */
    el3hlt = EL3HLT, /* 46: Level 3 halted */
    el3rst = EL3RST, /* 47: Level 3 reset */
    elnrng = ELNRNG, /* 48: Link number out of range */
    eunatch = EUNATCH, /* 49: Protocol driver not attached */
    enocsi = ENOCSI, /* 50: No CSI structure available */
    el2hlt = EL2HLT, /* 51: Level 2 halted */
    ebade = EBADE, /* 52: Invalid exchange */
    ebadr = EBADR, /* 53: Invalid request descriptor */
    exfull = EXFULL, /* 54: Exchange full */
    enoano = ENOANO, /* 55: No anode */
    ebadrqc = EBADRQC, /* 56: Invalid request code */
    ebadslt = EBADSLT, /* 57: Invalid slot */
    edeadlock = EDEADLK,
    ebfont = EBFONT, /* 59: Bad font file format */
    enostr = ENOSTR, /* 60: Device not a stream */
    enodata = ENODATA, /* 61: No data available */
    etime = ETIME, /* 62: Timer expired */
    enosr = ENOSR, /* 63: Out of streams resources */
    enonet = ENONET, /* 64: Machine is not on the network */
    enopkg = ENOPKG, /* 65: Package not installed */
    eremote = EREMOTE, /* 66: Object is remote */
    enolink = ENOLINK, /* 67: Link has been severed */
    eadv = EADV, /* 68: Advertise error */
    esrmnt = ESRMNT, /* 69: Srmount error */
    ecomm = ECOMM, /* 70: Communication error on send */
    eproto = EPROTO, /* 71: Protocol error */
    emultihop = EMULTIHOP, /* 72: Multihop attempted */
    edotdot = EDOTDOT, /* 73: RFS specific error */
    ebadmsg = EBADMSG, /* 74: Not a data message */
    eoverflow = EOVERFLOW, /* 75: Value too large for defined data type */
    enotuniq = ENOTUNIQ, /* 76: Name not unique on network */
    ebadfd = EBADFD, /* 77: File descriptor in bad state */
    eremchg = EREMCHG, /* 78: Remote address changed */
    elibacc = ELIBACC, /* 79: Can not access a needed shared library */
    elibbad = ELIBBAD, /* 80: Accessing a corrupted shared library */
    elibscn = ELIBSCN, /* 81: .lib section in a.out corrupted */
    elibmax = ELIBMAX, /* 82: Attempting to link in too many shared libraries */
    elibexec = ELIBEXEC, /* 83: Cannot exec a shared library directly */
    eilseq = EILSEQ, /* 84: Illegal byte sequence */
    erestart = ERESTART, /* 85: Interrupted system call should be restarted */
    estrpipe = ESTRPIPE, /* 86: Streams pipe error */
    eusers = EUSERS, /* 87: Too many users */
    enotsock = ENOTSOCK, /* 88: Socket operation on non-socket */
    enotsup = ENOTSUP, /* same value as EOPNOTSUPP */
    edestaddrreq = EDESTADDRREQ, /* 89: Destination address required */
    emsgsize = EMSGSIZE, /* 90: Message too long */
    eprototype = EPROTOTYPE, /* 91: Protocol wrong type for socket */
    enoprotoopt = ENOPROTOOPT, /* 92: Protocol not available */
    eprotonosupport = EPROTONOSUPPORT, /* 93: Protocol not supported */
    esocktnosupport = ESOCKTNOSUPPORT, /* 94: Socket type not supported */
    eopnotsupp = EOPNOTSUPP, /* 95: Operation not supported on transport endpoint */
    epfnosupport = EPFNOSUPPORT, /* 96: Protocol family not supported */
    eafnosupport = EAFNOSUPPORT, /* 97: Address family not supported by protocol */
    eaddrinuse = EADDRINUSE, /* 98: Address already in use */
    eaddrnotavail = EADDRNOTAVAIL, /* 99: Cannot assign requested address */
    enetdown = ENETDOWN, /* 100: Network is down */
    enetunreach = ENETUNREACH, /* 101: Network is unreachable */
    enetreset = ENETRESET, /* 102: Network dropped connection because of reset */
    econnaborted = ECONNABORTED, /* 103: Software caused connection abort */
    econnreset = ECONNRESET, /* 104: Connection reset by peer */
    enobufs = ENOBUFS, /* 105: No buffer space available */
    eisconn = EISCONN, /* 106: Transport endpoint is already connected */
    enotconn = ENOTCONN, /* 107: Transport endpoint is not connected */
    eshutdown = ESHUTDOWN, /* 108: Cannot send after transport endpoint shutdown */
    etoomanyrefs = ETOOMANYREFS, /* 109: Too many references: cannot splice */
    etimedout = ETIMEDOUT, /* 110: Connection timed out */
    econnrefused = ECONNREFUSED, /* 111: Connection refused */
    ehostdown = EHOSTDOWN, /* 112: Host is down */
    ehostunreach = EHOSTUNREACH, /* 113: No route to host */
    ealready = EALREADY, /* 114: Operation already in progress */
    einprogress = EINPROGRESS, /* 115: Operation now in progress */
    estale = ESTALE, /* 116: Stale file handle */
    euclean = EUCLEAN, /* 117: Structure needs cleaning */
    enotnam = ENOTNAM, /* 118: Not a XENIX named type file */
    enavail = ENAVAIL, /* 119: No XENIX semaphores available */
    eisnam = EISNAM, /* 120: Is a named type file */
    eremoteio = EREMOTEIO, /* 121: Remote I/O error */
    edquot = EDQUOT, /* 122: Quota exceeded */
    enomedium = ENOMEDIUM, /* 123: No medium found */
    emediumtype = EMEDIUMTYPE, /* 124: Wrong medium type */
    ecanceled = ECANCELED, /* 125: Operation Canceled */
    enokey = ENOKEY, /* 126: Required key not available */
    ekeyexpired = EKEYEXPIRED, /* 127: Key has expired */
    ekeyrevoked = EKEYREVOKED, /* 128: Key has been revoked */
    ekeyrejected = EKEYREJECTED, /* 129: Key was rejected by service */
    eownerdead = EOWNERDEAD, /* 130: Owner died */
    enotrecoverable = ENOTRECOVERABLE, /* 131: State not recoverable */
    erfkill = ERFKILL, /* 132: Operation not possible due to RF-kill */
    ehwpoison = EHWPOISON, /* 133: Memory page has hardware error */
};

using enum errc;

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const ec) noexcept
{
    return { ec, error_category() };
}

inline std::error_code
make_error_code(int const ec) noexcept
{
    return { ec, error_category() };
}

} // namespace stream9::linux

namespace std {

template<>
struct is_error_code_enum<stream9::linux::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_LINUX_ERROR_ERROR_CODE_HPP
