#ifndef STREAM9_LINUX_CORE_READV_HPP
#define STREAM9_LINUX_CORE_READV_HPP

#include "error_code.hpp"
#include "fd.hpp"
#include "iovec.hpp"
#include "namespace.hpp"

#include <stream9/array_view.hpp>
#include <stream9/number.hpp>
#include <stream9/outcome.hpp>

#include <sys/uio.h>

namespace stream9::linux {

inline outcome<natural<ssize_t>, lx::errc>
readv(fd_ref fd, array_view<iovec const> iov) noexcept
{
    auto n = ::readv(fd, iov.data(), iov.size());
    if (n == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_CORE_READV_HPP
