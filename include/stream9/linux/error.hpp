#ifndef STREAM9_LINUX_ERROR_ERROR_HPP
#define STREAM9_LINUX_ERROR_ERROR_HPP

#include "error_code.hpp"
#include "namespace.hpp"

#include <stream9/errors.hpp>

#endif // STREAM9_LINUX_ERROR_ERROR_HPP
