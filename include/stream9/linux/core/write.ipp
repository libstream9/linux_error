#ifndef STREAM9_LINUX_FILE_WRITE_IPP
#define STREAM9_LINUX_FILE_WRITE_IPP

#include "../namespace.hpp"

#include <unistd.h>

namespace stream9::linux {

inline outcome<natural<ssize_t>, lx::errc>
write(fd_ref fd, array_view<char const> buf) noexcept
{
    auto n = ::write(fd, buf.data(), buf.size());
    if (n == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

template<typename T>
outcome<natural<ssize_t>, lx::errc>
write(fd_ref fd, T& val) noexcept
    requires (!std::convertible_to<T, array_view<char const>>)
{
    auto n = ::write(fd, &val, sizeof(T));
    if (n == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_WRITE_IPP
