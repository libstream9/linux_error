#ifndef STREAM9_LINUX_FILE_JSON_FWD_HPP
#define STREAM9_LINUX_FILE_JSON_FWD_HPP

namespace stream9::json {

struct value_from_tag;
class value;

} // namespace stream9::json

#endif // STREAM9_LINUX_FILE_JSON_FWD_HPP
