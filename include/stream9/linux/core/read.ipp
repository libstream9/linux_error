#ifndef STREAM9_LINUX_FILE_READ_IPP
#define STREAM9_LINUX_FILE_READ_IPP

#include "../read.hpp"

#include <unistd.h>

namespace stream9::linux {

inline outcome<natural<ssize_t>, lx::errc>
read(fd_ref fd, array_view<char> buf) noexcept
{
    auto n = ::read(fd, buf.data(), buf.size());
    if (n == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

template<typename T>
outcome<natural<ssize_t>, lx::errc>
read(fd_ref fd, T& buf) noexcept
    requires (!std::convertible_to<T, array_view<char>>)
{
    auto len = sizeof(buf);
    auto n = ::read(fd, &buf, len);
    if (n == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_FILE_READ_IPP
