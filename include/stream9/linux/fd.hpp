#ifndef STREAM9_LINUX_FD_HPP
#define STREAM9_LINUX_FD_HPP

#include "core/json_fwd.hpp"
#include "namespace.hpp"

#include <compare>
#include <iosfwd>

#include <stream9/safe_integer.hpp>
#include <stream9/string.hpp>

namespace stream9::linux {

/*
 * @model std::movable
 * @model totally_ordered
 */
class fd
{
public:
    // essential
    explicit fd(safe_integer<int, 0> v) noexcept;

    ~fd() noexcept;

    fd(fd const&) = delete;
    fd& operator=(fd const&) = delete;

    fd(fd&& other) noexcept;
    fd& operator=(fd&& other) noexcept;

    // query
    int descriptor_flags() const; // fcntl(F_GETFD) see fcntl(2)
    int status_flags() const; // fcntl(F_GETFL) see. fcntl(2)

    bool close_on_exec() const; // descriptor_flags() & FD_CLOEXEC
    bool nonblocking_mode() const; // status_flags() & O_NONBLOCK
    int access_mode() const; // status_flags() & O_ACCMODE

    string path() const; // /proc/self/fd/*
    bool is_open() const; // fcntl(F_GETFD) != -1

    // modifier
    void set_descriptor_flags(int); // fcntl(F_SETFD) see fcntl(2)
    void set_status_flags(int); // fcntl(F_SETFL) see fcntl(2)

    void set_close_on_exec(bool); // FD_CLOEXEC
    void set_nonblocking_mode(bool); // O_NONBLOCK

    void close();
    int release() noexcept;

    // conversion
    operator int () const noexcept;

    // comparison
    bool operator==(fd const&) const = default;
    std::strong_ordering operator<=>(fd const&) const = default;

    bool operator==(int fd) const noexcept;
    std::strong_ordering operator<=>(int fd) const noexcept;

private:
    int m_fd = -1;
};

std::ostream& operator<<(std::ostream&, fd const&);

void tag_invoke(json::value_from_tag, json::value&, fd const&);

/*
 * @model std::regular
 * @model std::totally_ordered
 */
class fd_ref
{
public:
    // essential
    fd_ref() = default; // partially-formed

    fd_ref(fd const&) noexcept;
    fd_ref(int fd) noexcept;

    // query
    int descriptor_flags() const; // fcntl(F_GETFD) see fcntl(2)
    int status_flags() const; // fcntl(F_GETFL) see. fcntl(2)

    bool close_on_exec() const; // descriptor_flags() & FD_CLOEXEC
    bool nonblocking_mode() const; // status_flags() & O_NONBLOCK
    int access_mode() const; // status_flags() & O_ACCMODE

    string path() const; // /proc/self/fd/*
    bool is_open() const; // fcntl(F_GETFD) != -1

    // modifier
    void set_descriptor_flags(int); // fcntl(F_SETFD) see fcntl(2)
    void set_status_flags(int); // fcntl(F_SETFL) see fcntl(2)

    void set_close_on_exec(bool); // FD_CLOEXEC
    void set_nonblocking_mode(bool); // O_NONBLOCK

    void close();
    int release() noexcept;

    // conversion
    operator int () const noexcept;

    // comparison
    bool operator==(fd_ref const&) const = default;
    std::strong_ordering operator<=>(fd_ref const&) const = default;

    bool operator==(fd const&) const noexcept;
    std::strong_ordering operator<=>(fd const&) const noexcept;

    bool operator==(int fd) const noexcept;
    std::strong_ordering operator<=>(int fd) const noexcept;

private:
    int m_fd = -1;
};

std::ostream& operator<<(std::ostream&, fd_ref);

void tag_invoke(json::value_from_tag, json::value&, fd_ref);

/*
 * class fd
 */
inline fd::
fd(safe_integer<int, 0> v) noexcept
    : m_fd { v }
{}

inline fd::
fd(fd&& other) noexcept
    : m_fd { other.m_fd }
{
    other.m_fd = -1;
}

inline int fd::
release() noexcept
{
    int const fd = m_fd;
    m_fd = -1;
    return fd;
}

inline fd::
operator int () const noexcept
{
    return m_fd;
}

inline bool fd::
operator==(int const fd) const noexcept
{
    return m_fd == fd;
}

inline std::strong_ordering fd::
operator<=>(int const fd) const noexcept
{
    return m_fd <=> fd;
}

/*
 * class fd_ref
 */
inline fd_ref::
fd_ref(fd const& fd) noexcept
    : m_fd { fd }
{}

inline fd_ref::
fd_ref(int const fd) noexcept
    : m_fd { fd }
{}

inline fd_ref::
operator int () const noexcept
{
    return m_fd;
}

inline bool fd_ref::
operator==(fd const& fd) const noexcept
{
    return m_fd == fd;
}

inline std::strong_ordering fd_ref::
operator<=>(fd const& fd) const noexcept
{
    return m_fd <=> fd;
}

inline bool fd_ref::
operator==(int const fd) const noexcept
{
    return m_fd == fd;
}

inline std::strong_ordering fd_ref::
operator<=>(int const fd) const noexcept
{
    return m_fd <=> fd;
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_FD_HPP
