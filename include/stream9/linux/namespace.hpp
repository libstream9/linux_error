#ifndef STREAM9_LINUX_CORE_NAMESPACE_HPP
#define STREAM9_LINUX_CORE_NAMESPACE_HPP

namespace stream9 {

namespace linux {}

namespace st9 = stream9;
namespace lx = linux;

} // namespace stream9

#endif // STREAM9_LINUX_CORE_NAMESPACE_HPP
